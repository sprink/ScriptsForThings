import os, sys, time, secrets
import argparse

root_dir=os.getcwd()
root_contents=list(os.scandir(root_dir))

red='\033[31m'
green='\033[32m'
yellow='\033[33m'
blank='\033[0m'

class COUNT:
    def __init__(self):
        self.COUNT=0
        self.ALL_FILES = []
count=COUNT()

def main():
    parser=argparse.ArgumentParser(prog='PROG', usage='%(prog)s [options]')
    #parser.add_argument('-h', help='Program to shasum files and check against another directory')
    parser.add_argument('--algo', help='Hashing algorithm to use', nargs=1)
    parser.add_argument('--root-dir','-r', help='Set root directory', nargs=1)
    parser.add_argument('--output','-o', help='Set Output file name (default time.Output)', nargs=1)
    parser.add_argument('--compare','-c', help='Set Comparison Directory', nargs=1)

    F=parser.parse_args()
    print(F)
    if F.algo:
        ALGO=F.algo[0]
    else:
        ALGO='sha256'
    if F.root_dir:
        print(yellow+'root_dir set')
        root_dir= F.root_dir[0]
    file_hits=[]
    sub_dir_hits=[]
    for i,j,k in os.walk(root_dir):
        for x in j:
            #print(yellow+os.path.join(i,x)+blank)
            sub_dir_hits.append(x)
        if k!=[]:
            for y in k:
                #print(green+os.path.join(i,y)+blank)
                file_hits.append(os.path.join(i,y))

    if F.output:
        out_name = F.output[0]
    else:
        out_name = str(time.time())+'.Output'

    with open(out_name,'w') as f: 
        for hits in file_hits:
            #f.write(os.popen('sha1sum '+hits).read())      
            f.write(os.popen(ALGO+'sum '+hits).read())      
            #print(green+f'wrote {hits}'+blank)
        f.close()

    if F.compare:
        print(yellow+f'replace root_dir {root_dir} with compare dir {F.compare[0]}'+blank)
        with open(out_name, 'r') as f:
            changeme = f.read()
            f.close()

        changeme=changeme.replace(root_dir, F.compare[0])
        chk_name='chktmp'+secrets.token_urlsafe(8)
        with open(chk_name, 'w') as f:
            f.write(changeme)
            print(yellow+f'wrote {chk_name}'+blank)
            f.close()

        # put algo arg here
        #print(os.popen('sha1sum -c '+chk_name).read())
        res = os.popen(ALGO+'sum -c '+chk_name).read()
        colorize = res.split('\n')
        for i in range(len(colorize)):
            if colorize[i][-2:] == 'OK':
                colorize[i] = green+colorize[i]+blank
            else:
                colorize[i] = red+colorize[i]+blank
        print('\n'.join(colorize))

    return file_hits
    
if __name__=='__main__':
    clean = main()
